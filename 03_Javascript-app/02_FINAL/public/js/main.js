let datas = {};
let currentRoute = 'home';

/**
 * Setup the process of the loading the page 
 * within all specifications coming from the routes.json file
 * 
 * @returns {jqXHR}
 */
let dataRoute = function()
{
    currentRoute = location.hash.slice(1);
    
    let request = new XMLHttpRequest();
    
    request.open( 'GET', 'public/js/routes.json', true );

    request.onload = function() 
    {
        if( request.status >= 200 && request.status < 400 ){

            let jsonStr = JSON.parse( request.responseText );

            datas = jsonStr.routes[currentRoute];
            
            setInterface();
        }
    };
    
    request.onerror = function()
    {
        location.hash = currentRoute;
    };
    
    request.send();
};



/**
 * Sets the interface and integer ejs pages and loads JS
 * 
 *  @return void 
 **/

let setInterface = function()
{
    let scriptsLoaded = document.querySelectorAll( 'script.pagescript' );
    
    if( scriptsLoaded && scriptsLoaded.length > 0 )
    {
        scriptsLoaded.forEach( function( scriptLoaded )
        {
            scriptLoaded.parentNode.removeChild( scriptLoaded );
        });
    }
        
    loadViewContent( 'views/partials/header.ejs', 'header', function()
    { 
        loadViewContent( 'views/partials/footer.ejs', 'footer', function()
        {
            loadViewContent( 'views/pages/' + currentRoute + '.ejs', 'main', function()
            {
                if( datas.scripts && datas.scripts.length > 0 )
                {
                    loadJsContent( datas.scripts, 0, datas.scripts.length );
                }
            });
        });
    });
};


let loadViewContent = function( getUrl, htmlSection, callback )
{
    let request = new XMLHttpRequest();
    
    request.open('GET', getUrl, true);

    request.onload = function() 
    {
        if( request.status >= 200 && request.status < 400 ) 
        {
            let view = request.responseText;
            

            html = ejs.render( view, datas );
     
            document.querySelector( htmlSection ).innerHTML  = html;
            
            if( typeof callback !== 'undefined')
            {
                callback();
            }
        }
    };
      
    request.send();
};



let loadJsContent = function( scripts, nScript, nbScripts )
{
    let script = document.createElement('script');
                
    script.onload = function()
    {
        if( scripts[nScript][1] ) 
        {
            window[ scripts[nScript][1] ]( datas );
        }
        
        let nextScript = ( nScript + 1 );
        
        if( nextScript < nbScripts )
        {
            loadJsContent( scripts, nextScript, nbScripts );
        }
    };
    
    script.src = 'public/js/' + scripts[nScript][0] + '?' + new Date().getTime();

    script.classList.add( 'pagescript' );

    document.querySelector('body').appendChild( script );
};


/** Page loading - navigation **/

/**
 * Called when an anchor is set in the URL (ex. : #home)
 */
window.onhashchange = function()
{
    dataRoute();
};


window.onload = function()
{
    if( location.hash === '#' + currentRoute )
    {
        window.location = "";
    }
    
    location.hash = currentRoute;
};