<?php
header("Access-Control-Allow-Origin:*" );

$rssUrl = ( isset( $_GET[ 'rss' ] ) ) ? $_GET['rss'] : 'https://www.letemps.ch/feed';

$xml = simplexml_load_file( $rssUrl ); 

echo json_encode( $xml );