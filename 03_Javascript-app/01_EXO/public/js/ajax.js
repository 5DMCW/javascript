var rss = function()
{
    var request = new XMLHttpRequest();
    
    request.open( 'GET', datas.urlRSS, true );

    request.onload = function() 
    {
        if( request.status >= 200 && request.status < 400 ){
            
            var jsonRSS = JSON.parse( request.responseText );
            
            var rssRender = '';
            
            jsonRSS.channel.item.forEach(  function( item ){
                
                rssRender += '<li><a href="' + item.link + '">' + item.title + '</a></li>';

            });
            
            document.querySelector( 'ul.rss' ).innerHTML = rssRender;
        }
    };
    
    request.onerror = function()
    {
        console.log('error');
    };
    
    request.send();
};